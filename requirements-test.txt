redis==3.5.3
Flask==2.0.1
pytest==6.2.4
coverage==5.5
python-dotenv==0.19.0