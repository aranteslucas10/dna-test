# dna-test

[Application](gcp-gateway/swagger.yml)

## Dependencies

This project use:

* Program language [Python](https://python.org) 
* Google Cloud Provider [GCP](https://cloud.google.com/)
* [Docker](https://www.docker.com/) (Dev only, used for a local redis instance)

## The problem!

```text
Magneto wants to recruit as many mutants as possible so he can fight the X-Men.
He has hired you to develop a project that detects if a human is a mutant based on his DNA sequence.

For that he has asked you to create a program with a method or function with the following signature:

boolean isMutant (String [] dna); // Java example

Where you will receive as a parameter an array of Strings that represent each row of a table of
(NxN) with the DNA sequence.

The letters of the Strings can only be: (A, T, C, G), which
represents each nitrogenous base of DNA.

No-Mutant
A T G C G A
C A G T G C
T T A T T T
A G A C G G
G C G T C A
T C A C T G

Is-Mutant
A T G C G A
C A G T G C
T T A T G T
A G A A G G
C C C C T A
T C A C T G

You will know if a human is a mutant, if you find more than on e sequence of four equal letters,
obliquely, horizontally or vertically. Example (Mutant case):

String [] dna = {"ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"};
```

### Level 1

```text
Program (in any programming language) that complies with the method requested by Magneto.
```

#### Solution

Using `python` and writing a function `is_mutant`, inside file `mutant.py`, inside `dna` package. The function receives a `List[str]` DNA and return if DNA is mutant DNA or not. Additionally, exists a function to validate if DNA is valid DNA. It will be used on the application.  

### Level 2

```text
Create a REST API, host that API in a free cloud computing (Google App Engine, Amazon AWS, etc.), 
create the “/ mutant /” service where it can be detected if a human is a mutant by sending the DNA sequence 
through HTTP POST with a JSON which has the following format:

POST → / mutant / {“dna”: ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]}

In case of verifying a mutant, it should return an HTTP 200-OK, otherwise a 403-Forbidden
```

#### Solution

The first approach was using:

    * GCP Cloud Functions, one for mutant endpoint and other for stats endpoint
    * API Gateway
    * Memory Store with Redis.
  
The second approach, and finally, is using GCP App Engine, file `main.py` at the project root. At the `main.py` file was implemented the endpoint `/mutant`. 

### Level 3

```text
Append a database, which stores the DNA's verified with the API. Only 1 record per DNA.
Expose an extra endpoint “/ stats” that returns a JSON with the statistics of the DNA checks:

{
    "count_mutant_dna": 40, 
    "count_human_dna": 100, 
    "ratio": 0.4
} 

Bear in mind that the API can receive aggressive fluctuations traffic (Between 100 and 1 million requests per second).
Test-Automatic, Code coverage > 80%.
```

#### Solution

At the `main.py` file was implemented the `/stat` endpoint. This endpoint get values of mutant and human DNA and compute the ratio value (humans DNA / mutant DNA).  

### Running Test

To run the tests on Windows, open a powershell terminal on root project and execute:

```powershell
python -m venv .venv
.venv\Scripts\activate
python -m pip install -r .\requirements-test.txt
coverage run -m pytest -v
coverage report --omit .venv\*
```

### Running Locally

Execute a Redis container:

```powershell
docker run -it --rm -p 6379:6379 redis
```

Move `.env.example` file to `.env`, update, and then execute `python ./main.py`

```powershell
mv .env.example .env
python ./main.py
```

## Deploy on GCP App Engine

`On Windows`  

Move `app.yaml.example` file to `app.yaml` and update it.  

```powershell
mv app.yaml.example app.yaml
gcloud app create
gcloud app deploy app.yaml --project [PROJECT-NAME]
```
