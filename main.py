# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START gae_python38_app]
# [START gae_python3_app]

import os
import redis
import flask

from dna.mutant import is_mutant, is_valid_dna


redis_host = os.environ.get('REDISHOST', 'localhost')
redis_port = int(os.environ.get('REDISPORT', 6379))
redis_client = redis.StrictRedis(host=redis_host, port=redis_port)

# If `entrypoint` is not defined in app.yaml, App Engine will look for an app
# called `app` in `main.py`.
app = flask.Flask(__name__)


@app.route('/mutant', methods=['POST'])
def mutant():
    """Verify is a valid DNA, a mutant DNA and save DNA to Redis."""
    content = flask.request.json
    if content and 'dna' in content:
        dna = content['dna']
        if is_valid_dna(dna=dna):
            key = "".join(dna)
            if is_mutant(dna=dna):
                redis_client.set(f"dna:mutant:{key}", 1)
                # redis_client.incr('count_mutant_dna', 1)
                return "", 200

            redis_client.set(f"dna:human:{key}", 0)
            # redis_client.incr('count_human_dna', 1)

    return "", 403


@app.route('/stats', methods=['GET'])
def stats():
    """Return count of human DNA, mutant DNA and ratio."""
    mutants = len(redis_client.keys("dna:mutant:*"))
    humans = len(redis_client.keys('dna:human:*'))
    ratio = round(mutants / humans if humans >= 1 else 1, 2)
    
    try:
        ratio = round(mutants / humans, 2)
    except ZeroDivisionError:
        ratio = 1

    return {
        "count_mutant_dna": mutants , 
        "count_human_dna": humans, 
        "ratio": ratio
    }


if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
# [END gae_python3_app]
# [END gae_python38_app]