import argparse

from dna.mutant import is_valid_dna, is_mutant


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--dna', nargs="+", dest='dna', required=True, help="sequence of dna, ex.: ATG TGA GAT")
    args = parser.parse_args()
    if not is_valid_dna(args.dna):
        raise ValueError("DNA is not valid!")
    
    mutant = is_mutant(args.dna)
    if mutant:
        print(" Mutant detected! ".center(80, "="))
    else:
        print(" Just a normal people. ".center(80, "="))
