
from typing import Generator, List


VALID_LETTERS = ['A', 'T', 'C', 'G']


def is_valid_dna(dna: List[str]) -> bool:
    """Verify if DNA is a valid DNA.
    
    DNA letters needs to be NxN matrix and letters must in 
    the VALID_LETTERS list.
    
    Example 6x6 valid DNA:
    
    ["ATGCGA", 
     "CAGTGC",
     "TTATGT",
     "AGAAGG",
     "CCCCTA",
     "TCACTG"}

    Args:
        dna (List[str]): Strings represent each row of a table of 
        (NxN) with DNA sequence. Ex.: ['ATCG', 'TCGA', 'CGAT', 'GATC']

    Returns:
        bool: true if is valid DNA else false.
    """
    elements = len(dna)
    
    for row in dna:
        if len(row) != elements:
            return False
        for letter in row:
            if letter not in VALID_LETTERS:
                return False
            
    return True


def get_obliques(dna: List[str]) -> Generator:
    """Return obliques from DNA.

    Args:
        dna (List[str]): Strings represent each row of a table of 
        (NxN) with valid DNA sequence.

    Yields:
        [list[str]]: Each oblique sequence on DNA.
    """
    elements, seq_max = (len(dna), 4)
    sequence = "".join([seq for seq in dna])  # create unique string.
    reverse_sequence = "".join([seq[::-1] for seq in dna])  # create unique string, with horizontal reverse matrix.
    for letters in [sequence, reverse_sequence]:
        for side in [('col', range(elements - seq_max + 1)),  # get obliques upper main diagonal inclusive.
                     ('row', range(1, elements - seq_max + 1))]:  # get obliques below main diagonal exclusive.
            for e in side[1]:
                start = e if side[0] == 'col' else e * elements
                oblique = []
                while start < elements * elements:
                    oblique.append(letters[start])
                    start += elements + 1
                    if len(oblique) == elements - e:
                        break
                yield oblique

def is_mutant(dna: List[str]) -> bool:
    """Verify if DNA is a mutant DNA.
    
    Is a mutant DNA if exist more one sequence of four equal letters.

    Args:
        dna (List[str]): Strings represent each row of a table of 
        (NxN) with valid DNA sequence.

    Returns:
        bool: true if is mutant else false.
    """
    elements = len(dna)
    seq_max = 4
    find_sequences = 0
    sequences_limit = 2

    for row in range(elements):
        count, before = (None, None)
        for col in range(elements):
            letter = dna[row][col]
            if count is None:
                before, count = (letter, 1)
            else:
                if letter == before:
                    count += 1
                    if count == seq_max:
                        find_sequences += 1
                        count = None
                        if find_sequences >= sequences_limit:
                            return True
                else:
                    if col > elements - (seq_max - 1):
                        break
                    before, count = (letter, 1)
                    
    for col in range(elements):
        count, before = (None, None)
        for row in range(elements):
            letter = dna[row][col]
            if count is None:
                before, count = (letter, 1)
            else:
                if letter == before:
                    count += 1
                    if count == seq_max:
                        find_sequences += 1
                        count = None
                        if find_sequences >= sequences_limit:
                            return True
                else:
                    if row > elements - (seq_max - 1):
                        break
                    before, count = (letter, 1)

    for o in get_obliques(dna):
        count, before = (None, None)
        for i, letter in enumerate(o):
            if count is None:
                before, count = (letter, 1)
            else:
                if letter == before:
                    count += 1
                    if count == seq_max:
                        find_sequences += 1
                        count = None
                        if find_sequences >= sequences_limit:
                            return True
                else:
                    if i > len(o) - (seq_max - 1):
                        break
                    before, count = (letter, 1)

    return False
