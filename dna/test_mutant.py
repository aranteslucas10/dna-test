from dna.mutant import get_obliques, is_valid_dna, is_mutant


def test_is_valid_dna():
    assert True == is_valid_dna(["ATGCGA", "CAGTGC", "TTATTG", "AGAAGG", "CCTCTG", "TCGGGG"])
    
def test_is_not_valid_dna_letter():
    assert False == is_valid_dna(["ATGCGA", "CAGTGC", "TTATTG", "AGAAGG", "CCTCTG", "TCGGGP"])
    
def test_is_not_valid_dna_sequence_broke():
    assert False == is_valid_dna(["ATGCGA", "CAGTGC", "TTATTG", "AGAAGG", "CCTCTG", "TCGGG"])
    
def test_is_mutant():
    assert True == is_mutant(["AAAAAAAA", "CGGTCCGG", "TTATGTTT", "AGAAGGAG", "CTCCTAGG", "TCACTGAC", "AGTAGTCT", "TCACTGAC"])
    
def test_get_obliques():
    expected = [['A', 'A', 'A', 'A'], ['A', 'A', 'A', 'A']]
    assert expected == list(get_obliques(["ACCA", "CAAC", "CAAC", "ACCA"]))
    expected = [['A', 'C', 'A', 'A'], ['A', 'C', 'A', 'A']]
    assert expected == list(get_obliques(["ACCA", "CCCC", "CAAC", "ACCA"]))
    
def test_is_mutant_obliquely_main_diagonal():
    assert True == is_mutant(["ACCA", "CAAC", "CAAC", "ACCA"])
    assert False == is_mutant(["ACCA", "CCCC", "CAAC", "ACCA"])

def test_is_mutant_obliquely():
    assert True == is_mutant(["TTCGA", "GATCG", "CGATC", "TCGAT", "ATCGG"])
    assert False == is_mutant(["TTCGA", "GAACG", "CGATC", "TCGAT", "ATCGG"])

def test_is_mutant_best_case():
    assert True == is_mutant(["AAAAAA", "AAAAAA", "AAAAAA", "AAAAAA", "AAAAAA", "AAAAAA"])

def test_is_mutant_no_one_mutant_gene():
    assert False == is_mutant(["AAAAAAAG", "CGGTCCGG", "TTATGTTT", "AGAAGGAS", "CTCCTAGG",  "TCACTGAC", "AGTAGTCT", "TCACTGAC"])
    
def test_is_mutant_one_mutant_gene():
    assert False == is_mutant(["GGGGGA", "CAGTCC", "TTATGT", "AGAAGG", "CTCCTA", "TCACTG"])
